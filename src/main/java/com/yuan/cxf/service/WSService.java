package com.yuan.cxf.service;

public interface WSService {
    String sayHello(String name);
    String getInfo();
}
