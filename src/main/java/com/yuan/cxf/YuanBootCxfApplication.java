package com.yuan.cxf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YuanBootCxfApplication {
    public static void main(String[] args) {
        SpringApplication.run(YuanBootCxfApplication.class,args);
    }
}
