package com.yuan.cxf.controller;

import cn.com.webxml.ArrayOfString;
import cn.com.webxml.GetSupportDataSetResponse;
import cn.com.webxml.WeatherWebService;
import cn.com.webxml.WeatherWebServiceSoap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/net")
public class NetWeatherWebServiceController {
    /**
     * http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl
     * description: 调用.net发布的WebService服务_获取省份信息_无输入参数
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-2-5
     */
    @RequestMapping("/getWeather")
    public Object getWeather() {
        WeatherWebService weatherWebService = new WeatherWebService();
        WeatherWebServiceSoap weatherWebServiceSoap = weatherWebService.getWeatherWebServiceSoap();
        ArrayOfString supportProvince = weatherWebServiceSoap.getSupportProvince();
        List<String> stringList = supportProvince.getString();
        return stringList;
    }
    /**
     * http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl
     * description: 调用.net发布的WebService服务_获取省份信息_有一个输入参数
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-2-5
     */
    @RequestMapping("/getSupportCity")
    public Object getSupportCity(String byProvinceName) {
        WeatherWebService weatherWebService = new WeatherWebService();
        WeatherWebServiceSoap weatherWebServiceSoap = weatherWebService.getWeatherWebServiceSoap();
        ArrayOfString supportCity = weatherWebServiceSoap.getSupportCity(byProvinceName == null ? "ALL" : byProvinceName);
        List<String> stringList = supportCity.getString();
        return stringList;
    }



    /**
     * http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl
     * description: 调用.net发布的WebService服务_获取省份信息_有一个输入参数
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-2-5
     */
    @RequestMapping("/getSupportDataSet")
    public Object getSupportDataSet() {
        WeatherWebService weatherWebService = new WeatherWebService();
        WeatherWebServiceSoap weatherWebServiceSoap = weatherWebService.getWeatherWebServiceSoap();
        GetSupportDataSetResponse.GetSupportDataSetResult supportDataSet = weatherWebServiceSoap.getSupportDataSet();
        List<Object> any = supportDataSet.getAny();
        return any;
    }


   /* @RequestMapping("/getSupportCity1")
    public Object getSupportCity1(@PathVariable(value = "byProvinceName",required = false) String byProvinceName) {
        WeatherWebService weatherWebService = new WeatherWebService();
        WeatherWebServiceSoap weatherWebServiceSoap = weatherWebService.getWeatherWebServiceSoap();
        ArrayOfString supportCity = weatherWebServiceSoap.getSupportCity(byProvinceName == null ? "ALL" : byProvinceName);
        List<String> stringList = supportCity.getString();
        return stringList;
    }*/



}
