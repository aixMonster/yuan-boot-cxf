package com.yuan.cxf.controller;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jaxws")
public class JaxWsDynamicClientController {
    @RequestMapping("/client")
    public String jaxWsDynamicClient() {
        JaxWsDynamicClientFactory clientFactory = JaxWsDynamicClientFactory.newInstance();
        Client client = clientFactory.createClient("http://localhost:8888/ws?wsdl");
        Object[] objects;
        String result = null;
        try {
            objects = client.invoke("sayHello", new Object[]{"张三"});
            if (objects != null) {
                result = (String) objects[0];
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
