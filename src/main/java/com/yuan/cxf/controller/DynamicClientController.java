package com.yuan.cxf.controller;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.dynamic.DynamicClientFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dynamic")
public class DynamicClientController {
    /**
     * description: 获取用户信息
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-2-5
     */
    @RequestMapping("/getUserInfo")
    public String getUserInfo() {
        DynamicClientFactory clientFactory = DynamicClientFactory.newInstance();
        Client client = clientFactory.createClient("http://localhost:8101/ws/autoUserInfo?wsdl");
        Object[] objects;
        String result = null;
        try {
            objects = client.invoke("getInfo", new Object[]{});
            if (objects != null) {
                result = (String) objects[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * description: 获取组织信息
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-2-5
     */
    @RequestMapping("/getDeptInfo")
    public String getDeptInfo() {
        DynamicClientFactory clientFactory = DynamicClientFactory.newInstance();
        Client client = clientFactory.createClient("http://localhost:8101/ws/autoDeptInfo?wsdl");
        Object[] objects;
        String result = null;
        try {
            objects = client.invoke("getInfo", new Object[]{});
            if (objects != null) {
                result = (String) objects[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    /**
     * description: 动态调用.net发布的WebService接口，并获取信息
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-2-5
     */
    @RequestMapping("/getNetInfo")
    public String getNetInfo() {
        DynamicClientFactory clientFactory = DynamicClientFactory.newInstance();
        Client client = clientFactory.createClient("http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl");
        Object[] objects;
        String result = null;
        try {
            objects = client.invoke("getSupportProvince", new Object[]{});
            if (objects != null) {
                result = (String) objects[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
