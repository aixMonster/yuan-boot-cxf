package com.yuan.cxf.config;

import com.yuan.cxf.service.impl.DeptInfoWSServiceImpl;
import com.yuan.cxf.service.impl.HelloServiceImpl;
import com.yuan.cxf.service.impl.UserInfoWSServiceImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import javax.xml.ws.Endpoint;

/**
 * description: CXF 配置 服务小于三个可以这样配置，多了的话参考
 *
 * @param
 * @return String
 * @author:jinshengyuan
 * @date: 2022-1-30
 */
//@Configuration
public class CxfConfig {
    @Autowired
    private Bus bus;

    @Autowired
    private HelloServiceImpl helloService;

    @Autowired
    private UserInfoWSServiceImpl userInfoWSService;

    @Autowired
    private DeptInfoWSServiceImpl deptInfoWSService;

    /**
     * description: hello webService服务
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-1-30
     */
    @Bean
    public Endpoint helloServer() {
        EndpointImpl endpoint = new EndpointImpl(bus, helloService);
        endpoint.publish("/hello");
        return endpoint;
    }

    /**
     * description: 用户信息 WebService服务
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-1-30
     */
    @Bean
    public Endpoint userInfo() {
        EndpointImpl endpoint = new EndpointImpl(bus, userInfoWSService);
        endpoint.publish("/userInfo");
        return endpoint;
    }

    /**
     * description: 组织信息webservice服务
     *
     * @param
     * @return String
     * @author:jinshengyuan
     * @date: 2022-1-30
     */
    @Bean
    public Endpoint orgInfo() {
        EndpointImpl endpoint = new EndpointImpl(bus, deptInfoWSService);
        endpoint.publish("/orgInfo");
        return endpoint;
    }
}
